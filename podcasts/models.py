from django.db import models


class Podcast(models.Model):
    title = models.CharField(max_length=200, default='Trójkowy Ossobowy')
    aired = models.DateField('aired date', unique=True)
    audio_url = models.URLField('podcast audio url', blank=True)
    website = models.URLField('original podcast website', blank=True)

    def __str__(self):
        return self.title + " " + self.aired.strftime('%Y.%m.%d')

    def get_absolute_url(self):
        return "/podcasts/%i" % self.id
