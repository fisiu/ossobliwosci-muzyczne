from datetime import date, datetime

from django.contrib.syndication.views import Feed
# from django.urls import reverse
from django.utils.feedgenerator import Atom1Feed, DefaultFeed

from .models import Podcast


class CorrectMimeTypeFeedRss(DefaultFeed):
    content_type = "application/xml; charset=utf-8"


class CorrectMimeTypeFeedAtom(Atom1Feed):
    content_type = "application/xml; charset=utf-8"


class LastPodcastsFeedRss(Feed):
    title = "Trójkowy Ossobowy"
    link = "/podcasts/"
    description = "Trójkowy ossobowy - folkowo i metalowo. Audycja Wojciecha Ossowskiego."
    feed_url = "/podcasts/rss/"
    feed_type = CorrectMimeTypeFeedRss

    def items(self):
        return Podcast.objects.order_by('-aired')[:5]

    def item_author_name(self):
        return "Wojciech Ossowski"

    def item_title(self, item):
        return str(item)

    def item_description(self, item):
        return str(item)

    def item_enclosure_url(self, item):
        return item.audio_url

    def item_enclosure_mime_type(self):
        return "audio/mpeg"

    def item_pubdate(self, item):
        return datetime.fromisoformat(str(item.aired))

    # # item_link is only needed if NewsItem has no get_absolute_url method.
    # def item_link(self, item):
    #     return reverse('podcast-item', args=[item.pk])


class LastPodcastsFeedAtom(LastPodcastsFeedRss):
    feed_type = CorrectMimeTypeFeedAtom
    subtitle = LastPodcastsFeedRss.description
