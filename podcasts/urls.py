from django.urls import path

from . import views
from .feeds import LastPodcastsFeedAtom, LastPodcastsFeedRss

urlpatterns = [
    path('', views.index, name='index'),
    path('podcasts/atom/', LastPodcastsFeedAtom(), name='podcasts-feed-atom'),
    path('podcasts/rss/', LastPodcastsFeedRss(), name='podcasts-feed-rss')
]
